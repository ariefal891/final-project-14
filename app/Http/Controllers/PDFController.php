<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Cast;

class PDFController extends Controller
{
   
    public function exportPDF() 
    {
         $cast = Cast::all();
        $pdf = PDF::loadView('cast.laporan', compact('cast'));
        return $pdf->download('laporan.pdf');
    }
}
