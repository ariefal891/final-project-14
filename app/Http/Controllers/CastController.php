<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
use RealRashid\SweetAlert\Facades\Alert;

class CastController extends Controller
{


    public function index()
    {
        $cast = Cast::all();
        //dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function create(){
        return view('cast.create');
    }

    public function save(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = new Cast;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();
        Alert::success('Berhasil', 'Berhasil Tambah Data Cast');

        return redirect('/');
    }

    

    public function show($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $cast = Cast::where('id', $cast_id)->first();

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();
        Alert::success('Update', 'Data Cast Berhasil Diupdate');

        return redirect('/');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::where('id', $cast_id);
        $cast->delete();
        Alert::success('Hapus', 'Data Cast Berhasil Dihapus');
        return redirect('/');
    }

}
