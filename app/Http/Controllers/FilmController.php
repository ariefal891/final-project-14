<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
     
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact ('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = DB::table('genre')->get();
        // dd($genres);
        
        return view('film.create', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required|unique:film|max:255',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'genre_id' => 'required',
        ],
        [
            'judul.required' => 'Film Harus memiliki Judul',
            'ringkasan.required' => 'Ringkasan Film tidak boleh kosong, karakter tidak boleh lebih dari 255',
            'tahun.required' => 'Tahun film tidak boleh kosong',
            'poster.required' => 'Poster awal Film tidak boleh kosong',
            'genre_id' => 'Genre Tidak boleh kosong'
        ]);
        $PosterName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('poster'), $PosterName);


        $film = new Film;
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $PosterName;
        $film->genre_id = $request->genre_id;
 
        $film->save();
        Alert::success('Berhasil', 'Tambah Data Film Berhasil');
        return redirect ('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);

        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genres = DB::table('genre')->get();
        $film = Film::findOrFail($id);
        

        return view('film.edit', compact('film','genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required|max:255',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,jpg,png|max:2048',
            'genre_id' => 'required',
        ],
        [
            'judul.required' => 'Film Harus memiliki Judul',
            'ringkasan.required' => 'Ringkasan Film tidak boleh kosong, karakter tidak boleh lebih dari 255',
            'tahun.required' => 'Tahun film tidak boleh kosong',
            'poster.required' => 'Poster awal Film tidak boleh kosong',
            'genre_id' => 'Genre Tidak boleh kosong'
        ]);
        
        $film = Film::find($id);
        if($request->has('poster')){
            $PosterName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('poster'), $PosterName);
    
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->poster = $PosterName;
            $film->genre_id = $request->genre_id;
        }else{
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            #$film->poster = $PosterName;
            $film->genre_id = $request->genre_id;
        }

        

        $film->update();
        Alert::success('Update', 'Data film berhasil di update');
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $film = Film::find($id);
     
     $path = "poster/"; 
     File::delete($path . $film->poster);
     $film->delete();
     Alert::success('Delete', 'Data film berhasil di Delete');
     return redirect('/');
    }
}
