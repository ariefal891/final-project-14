@extends('layout.master')

@section('judul')
Register
@endsection

@section('content')
<h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="akhir"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" value="1" name="lk">Male<br>
        <input type="radio" value="2" name="lk">Female<br><br>

        <label>Nationality</label><br><br>
        <select name ="nation">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="brunei">Brunei</option>
        </select><br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bind">Bahasa Indonesia<br>
        <input type="checkbox" name="bing">English<br>
        <input type="checkbox" name="both">Other<br><br>

        <label>Bio</label><br><br>
        <textarea name+"message" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="kirim">

    </form>
@endsection

