@extends('layout.master')

@section('judul')
Halaman List Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-secondary btn-sm mb-3">Tambah Genre</a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Genre</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
       <tr>
         <td>{{$key + 1}}</td>
         <td>{{$item->nama}}</td>
         <td>
             
             <form action="/genre/{{$item->id}}" method="POST">
             <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
             <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                 @csrf
                 @method('delete')
                 <input type="submit" class="btn btn-danger btn-sm" value="Delete">
             </form>
         </td>
       </tr>
    @empty
      <h2>Data Tidak Ada</h2>
    @endforelse
  </tbody>
</table>

@endsection