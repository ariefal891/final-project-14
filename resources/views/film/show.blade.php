@extends('layout.master')

@section('judul')
Halaman detail Film {{$film->judul}}
@endsection

@section('content')
<img src="{{asset('poster/' . $film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>
<p>{{$film->tahun}}</p>
<p>{{$film->genre->nama}}</p>
<a href="/film" class="btn-secondary">Kembali</a>

@endsection