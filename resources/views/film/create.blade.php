@extends('layout.master')

@section('judul')
Tambah Film
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/omhd2gilrmvddqsxfbp404rchzalxgplk41j11acblmxrouq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mode : "textareas",
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : '',
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush
@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
<form action="/film/store" method="post" enctype="multipart/form-data">
    @csrf
   <div class="form-group">
    <label>Judul </label>
    <input type="text" name="judul" class="form-control">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" class="form-control"></textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Tahun</label><br>
    <input type="number" name="tahun" class="form-control">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Genre </label><br>
    <select name="genre_id" class="js-example-basic-single" style="width: 100%" id="">
    <option value="">---- Silahkan Pilih Genre ----</option>
    @foreach ($genres as $genre)
        <option value="{{$genre->id}}">{{$genre->nama}}</option>
    @endforeach
    </select>
  </div>
  @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Poster</label><br>
    <input type="file" name="poster" class="form-control">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection