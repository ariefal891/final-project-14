@extends('layout.master')

@section('judul')
Halaman List FILM
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-primary btn-sm mb-3">Tambah Data Film</a>
@endauth
<div class="row">
@forelse ($film as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;" >
            <img class="card-img-top" src="{{asset('poster/'. $item->poster)}}" alt="" height="400 px">
            <div class="card-body">
              <h3 >{{$item->judul}}</h3>
              <p class="card-text"> {{Str::limit($item->ringkasan, 30)}}</p>
              <p class="card-text">{{$item->genre->nama}}</p>
              <p class="card-tect"> {{$item->tahun}}</p>
              @auth
              <form action="{{ url('/film', $item->id) }}" method="post">
                @csrf
                @method('delete')
                <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm " >
              </form>
              @endauth

              @guest
              <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
              @endguest
              
            </div>
          </div>
    </div>
    @empty
    <h4>Data Belum Ada</h4>
    @endforelse
</div>




@endsection