@extends('layout.master')

@section('judul')
Halaman Edit Film {{$film->judul}}
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/omhd2gilrmvddqsxfbp404rchzalxgplk41j11acblmxrouq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mode : "textareas",
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : '',
  });
</script>
@endpush
@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Judul </label>
    <input type="text" value="{{$film->judul}}" name="judul" class="form-control">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Ringkasan/Synopsis Film</label>
    <textarea name="ringkasan"  class="form-control">{{$film->ringkasan}}</textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Tahun</label><br>
    <input type="number" value="{{$film->tahun}}" name="tahun" class="form-control">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Genre </label><br>
    <select name="genre_id" class="form-control" id="">
    <option value="">---- Silahkan Pilih Genre ----</option>
    @foreach ($genres as $genre)
        @if ($genre->id === $film->genre_id)
        <option value="{{$genre->id}}" selected>{{$genre->nama}}</option>
        @else
        <option value="{{$genre->id}}">{{$genre->nama}}</option>
        @endif
        
    @endforeach
    </select>
  </div>
  @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Poster</label><br>
    <input type="file" name="poster" class="form-control">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection