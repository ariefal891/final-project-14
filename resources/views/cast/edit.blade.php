@extends('layout.master')

@section('judul')
Edit Cast
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/omhd2gilrmvddqsxfbp404rchzalxgplk41j11acblmxrouq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mode : "textareas",
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : '',
  });
</script>
@endpush
@section('content')
   
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Nama : </label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label><br>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection