
<!DOCTYPE html>
<html>
<head>
	<title>Laporan Cast</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Cast</h4>
	</center>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Biodata</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
       <tr>
         <td>{{$key + 1}}</td>
         <td>{{$item->nama}}</td>
         <td>{{$item->umur}}</td>
         <td>{{$item->bio}}</td>
          <td>
         </td>
       </tr>
    @empty
      <h2>Data Tidak Ada</h2>
    @endforelse
  </tbody>
</table>

</body>
</html>

