@extends('layout.master')

@section('judul')
List Cast
@endsection

@section('content')

@auth
  <a href="/cast/create" class="btn btn-success btn-sm mb-3">Tambah Cast</a>
  <a href="/export" class="btn btn-info btn-sm mb-3" >Cetak PDF</a>
@endauth

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
       <tr>
         <td>{{$key + 1}}</td>
         <td>{{$item->nama}}</td>
         <td>{{$item->umur}}</td>
         <td>{{$item->bio}}</td>
         <td>
           
         @auth
             <form action="/cast/{{$item->id}}" method="POST">
             <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
             <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                 @csrf
                 @method('delete')
                 <input type="submit" class="btn btn-danger btn-sm" value="Delete">
             </form>

         @endauth

         @guest
         <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
         @endguest
            
         </td>
       </tr>
    @empty
      <h2>Data Tidak Ada</h2>
    @endforelse
  </tbody>
</table>


@endsection