<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::group(['middleware' => ['auth']], function () {

// CRUD Cast
    Route::get('/cast/create', 'CastController@create');//Route menuju tambah cast
    Route::post('/cast/save', 'CastController@save');//Route simpan data cast
    Route::get('/listcast', 'CastController@index'); //Route list cast
    Route::get('/cast/{cast_id}', 'CastController@show');//Route detail cast
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');//Route menuju edit cast
    Route::put('/cast/{cast_id}', 'CastController@update');//Route edit cast
    Route::delete('/cast/{cast_id}', 'CastController@destroy');//Route delete cast
    Route::get('/export', 'PDFController@exportPDF');

    //CRUD Genre
    Route::get('/genre/create', 'GenreController@create'); //Route Menuju tambah genre baru
    Route::post('/genre', 'GenreController@store'); //Route menambahkan data genre di database
    Route::get('/genre', 'GenreController@index'); //Route list genre
    Route::get('/genre/{genre_id}', 'GenreController@show'); // Route detail genre
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit');// Route edit genre
    Route::put('/genre/{genre_id}', 'GenreController@update'); //Route edit genre
    Route::delete('/genre/{genre_id}', 'GenreController@destroy');// Route delete genre

    //Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
 
});   
    

   Auth::routes();

   // CRUD Film
   //Route::resource('/film', 'FilmController'); // Route menuju tambah Film
   Route::get('/', 'FilmController@index'); //Route list film
   Route::get('/film/create', 'FilmController@create'); //Route Menuju tambah film baru
   Route::post('/film/store', 'FilmController@store'); //Route Tambah Film
   Route::get('/film/{film_id}', 'FilmController@show'); // Route detail film
   Route::get('/film/{film_id}/edit', 'FilmController@edit');// Route edit film
   Route::put('/film/{film_id}', 'FilmController@update'); //Route edit film
   Route::delete('/film/{film_id}', 'FilmController@destroy');// Route delete film
   // Route::post('/film','FilmController@store'); // Route menuju simpan data film


